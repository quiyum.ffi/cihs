<?php
session_start();
include_once "vendor/autoload.php";
use App\Members;
$object=new Members();
$oneData=$object->view($_SESSION['last_id']);
if($oneData->passing_year>=2002 && $oneData->passing_year<=2010){
    $passing_year=$oneData->passing_year;
    $amount=1000;
}
else if($oneData->passing_year>=2011 && $oneData->passing_year<=2017){
    $amount=500;
    $passing_year=$oneData->passing_year;
}
else if($oneData->passing_year>=0000){
    $amount=200;
    $passing_year="Regular";
}
$date=$oneData->registration_date;
$new=date('Y-m-d H:i:s',strtotime('+72 hour +0 minutes',strtotime($date)));
$newdate=date('d/m/Y h:i:s a', strtotime($new));
$date = date("d", strtotime("$oneData->birth_date"));
$month = date("m", strtotime("$oneData->birth_date"));
$year = date("Y", strtotime("$oneData->birth_date"));
if($month=='01'){
    $monthD='January';
}
if($month=='02'){
    $monthD='February';
}
else if($month=='03'){
    $monthD='March';
}
else if($month=='04'){
    $monthD='April';
}
else if($month=='05'){
    $monthD='May';
}
else if($month=='06'){
    $monthD='June';
}
else if($month=='07'){
    $monthD='July';
}
else if($month=='08'){
    $monthD='August';
}
else if($month=='09'){
    $monthD='September';
}
else if($month=='10'){
    $monthD='October';
}
else if($month=='11'){
    $monthD='November';
}
else if($month=='12'){
    $monthD='December';
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title>$title;</title>
    <link href="resources/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>

<body class="home-page">
<div class="wrapper">
    <div class="content container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2">
                    <img src="resources/banner.jpg" class="img-responsive img-rounded">
                    <h4 style="text-align: center">Reunion-2018</h4>
                    <h5 style="text-align: center">(Date: 06<sup>th</sup> January, 2018)</h5>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div style="width: 100%;overflow: hidden">
                    <div style="width: 30%;float: left">
                        <img src="resources/members_photo/<?php echo $oneData->picture?>" class="img-responsive img-rounded">
                    </div>
                    <div style="width: 70%;float: right">
                        <h5><?php echo $oneData->name?></h5>
                        <h5><?php echo $oneData->contact?></h5>
                        <h5><?php echo $oneData->email?></h5>
                        <h5><?php echo $oneData->passing_year?></h5>
                        <h5><?php echo $oneData->father_name?></h5>
                    </div>
                </div>

                <div style="width: 100%;overflow: hidden">
                    <table border="2px black solid" style="width: 100%; border-collapse: collapse">
                        <tr>
                            <td>Father's Name</td>
                            <td style="text-align: right">Mother's Name</td>
                        </tr>
                        <tr>
                            <td>Father's Name</td>
                            <td>Mother's Name</td>
                        </tr>
                        <tr>
                            <td>Father's Name</td>
                            <td>Mother's Name</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div><!--//content-->
</div><!--//wrapper-->
</body>
</html>