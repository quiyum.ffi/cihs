<?php
require_once("../vendor/autoload.php");
include ("../templateLayout/templateInformation.php");
session_start();
use App\Admin;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==0){
    $auth= new Admin();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../login.php');
        return;
    }
}
else {
    Utility::redirect('../login.php');
}

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("../templateLayout/css/meta.php");?>
    <?php include ("../templateLayout/css/templateCss.php");?>
    <?php include('../templateLayout/css/tableCss.php');?>
    <style>
        #profile-image1 {
            cursor: pointer;

            width: 100px;
            height: 100px;
            border:2px solid #03b1ce ;}
        .tital{ font-size:16px; font-weight:500;}
        .bot-border{ border-bottom:1px #f8f8f8 solid;  margin:5px 0  5px 0}
    </style>
</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include ("../templateLayout/adminNavigation.php");?>

    <!-- ******CONTENT****** -->
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left">Admin Profile</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">You are here:</li>
                        <li><a href="#">Home</a><i class="fa fa-angle-right"></i></li>
                        <li><a href="#">Admin</a><i class="fa fa-angle-right"></i></li>
                        <li class="current">Admin Profile</li>

                    </ul>
                </div><!--//breadcrumbs-->
            </header>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <?php
                    if(isset($_SESSION) && !empty($_SESSION['message'])) {

                        $msg = Message::getMessage();

                        echo "
                        <p id='message' style='text-align: center; font-family:Century Gothic;color: red;font-size: 14px;font-weight: 600;'>$msg</p>";
                        
                    }

                    ?>

                    
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <h2 style="text-align: center">ব্যক্তিগত তথ্য</h2>
                    </div>
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-7">

                                        <div class="panel panel-default">
                                            <div class="panel-heading">  <h4 >Admin's Profile</h4></div>
                                            <div class="panel-body">
                                                <div class="box box-info">
                                                    <div class="box-body">
                                                        <div class="col-sm-12">
                                                            <div  align="center"> <img alt="User Pic" src="../../resources/admin_photos/<?php echo $oneData->picture;?>" id="profile-image1" class="img-circle img-responsive">
                                                            </div>
                                                            <br>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <h4 style="color:#00b1b1;">System Admin</h4></span>
                                                            <span><p>Tarunno, Chittagong</p></span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <hr style="margin:5px 0 5px 0;">
                                                        <a href="<?php echo base_url?>views/admin/resetPassword.php?id=<?php echo $oneData->id?>" class="btn btn-primary">Reset Password</a>
                                                        <a href="<?php echo base_url?>views/admin/changeProfilePic.php?id=<?php echo $oneData->id?>" class="btn btn-primary">Change Profile Picture</a>
                                                        <hr style="margin:5px 0 5px 0;">

                                                        <div class="col-sm-5 col-xs-6 tital " >Name</div><div class="col-sm-7 col-xs-6 "><?php echo $oneData->name?></div>
                                                        <div class="clearfix"></div>
                                                        <div class="bot-border"></div>
                                                        <div class="col-sm-5 col-xs-6 tital " >Contact</div><div class="col-sm-7 col-xs-6 "><?php echo $oneData->contact?></div>
                                                        <div class="clearfix"></div>
                                                        <div class="bot-border"></div>
                                                        <div class="col-sm-5 col-xs-6 tital " >Email</div><div class="col-sm-7 col-xs-6 "><?php echo $oneData->email?></div>
                                                        <div class="clearfix"></div>
                                                        <div class="bot-border"></div>
                                                        
                                                    </div>
                                                    <!-- /.box -->

                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>

                </div>
            </div>
        </div>
    </div>
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include ("../templateLayout/footer.php");?>


<?php include ("../templateLayout/script/templateScript.php");?>
<?php include('../templateLayout/script/tableScript.php');?>
</body>
</html>

