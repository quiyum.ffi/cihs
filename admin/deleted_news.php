<?php
session_start();
require_once "../vendor/autoload.php";
include("../templateLayout/templateInformation.php");
use App\Admin;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==0){
    $auth= new Admin();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../login.php');
        return;
    }
}
else {
    Utility::redirect('../login.php');
}
use App\News;
$object=new News();
$deletedNews=$object->showdeletedNews();
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("../templateLayout/css/meta.php");?>
    <?php include("../templateLayout/css/templateCss.php");?>

</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include("../templateLayout/adminNavigation.php");?>
    <!-- ******CONTENT****** -->
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left">Deleted News</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">You are here:</li>
                        <li><a href="admin_profile.php">Home</a><i class="fa fa-angle-right"></i></li>
                        <li><a href="#">Admin</a><i class="fa fa-angle-right"></i></li>
                        <li class="current">Deleted news</li>
                    </ul>
                </div><!--//breadcrumbs-->
            </header>
            <div class="page-content">
                <div class="row page-row">
                    <?php
                    if(isset($_SESSION) && !empty($_SESSION['message'])) {

                        $msg = Message::getMessage();

                        echo "
                       <p id='message' style='text-align: center; font-family:Century Gothic;color: red;font-size: 14px;font-weight: 600;'>$msg</p>";

                    }

                    ?>
                    <div class="news-wrapper col-md-12 col-sm-12 col-xs-12">
                        <?php
                        foreach ($deletedNews as $top){
                            $date = date("d", strtotime("$top->date"));
                            $month = date("m", strtotime("$top->date"));
                            $year = date("Y", strtotime("$top->date"));
                            if($month=='01'){
                                $monthD='January';
                            }
                            if($month=='02'){
                                $monthD='February';
                            }
                            else if($month=='03'){
                                $monthD='March';
                            }
                            else if($month=='04'){
                                $monthD='April';
                            }
                            else if($month=='05'){
                                $monthD='May';
                            }
                            else if($month=='06'){
                                $monthD='June';
                            }
                            else if($month=='07'){
                                $monthD='July';
                            }
                            else if($month=='08'){
                                $monthD='August';
                            }
                            else if($month=='09'){
                                $monthD='September';
                            }
                            else if($month=='10'){
                                $monthD='October';
                            }
                            else if($month=='11'){
                                $monthD='November';
                            }
                            else if($month=='12'){
                                $monthD='December';
                            }
                            $demoHeadline=substr( $top->headline,0,90);


                            ?>
                            <div class="col-md-3 col-sm-3 col-xs-6">
                                <div class="item">
                                    <div class="news-item">
                                        <img class="thumb" src="../resources/news/title/<?php echo $top->title_pic?>" alt="" width="100%"/>
                                        <div style="min-height: 10vh">
                                            <h5 class="title"><a href=""><br><?php echo $demoHeadline?></a></h5>
                                        </div>

                                        <p><?php echo $date; if($date==01){echo "<sup>st</sup>";} elseif($date==31){echo "<sup>st</sup>";} elseif($date==2){echo "<sup>nd</sup>";}elseif($date==3){echo "<sup>rd</sup>";} else{echo "<sup>th</sup>";} ?><?php echo " ".$monthD." ".$year?></p>
                                        <p>Posted by: <a><?php echo $top->admin_name?></a></p>
                                        <a class="read-more" href="news_details.php?news_id=<?php echo $top->id?>">Read more<i class="fa fa-chevron-right"></i></a>
                                        <br>
                                    </div><!--//news-item-->

                                </div>
                            </div>
                            <?php
                        }
                        ?>

                    </div><!--//news-wrapper-->

                </div><!--//page-row-->
            </div><!--//page-content-->
        </div><!--//page-->
    </div><!--//content-->
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include("../templateLayout/footer.php");?>


<?php include("../templateLayout/script/templateScript.php");?>

</body>
</html>

