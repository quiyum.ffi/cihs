<?php
session_start();
require_once("../../vendor/autoload.php");
include ("../templateLayout/templateInformation.php");
use App\Admin;
use App\Utility\Utility;
use App\Message\Message;
if($_SESSION['role_status']==0){
    $auth= new Admin();
    $status = $auth->prepareData($_SESSION)->logged_in();

    if(!$status) {
        Utility::redirect('../login.php');
        return;
    }
}
else {
    Utility::redirect('../login.php');
}
use App\News;
$objNews=new News();
$objNews->setData($_GET);
$oneNews=$objNews->showSingleData();
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("../templateLayout/css/meta.php");?>
    <?php include ("../templateLayout/css/templateCss.php");?>
    <style>
        .caption{
            text-align: center;
            font-size: 12px;
        }
        .btn{
            font-size: 11px !important;
        }
    </style>
</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include ("../templateLayout/adminNavigation.php");?>
    <!-- ******CONTENT****** -->
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left">Change News Cover Photo</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">You are here:</li>
                        <li><a href="#">Home</a><i class="fa fa-angle-right"></i></li>
                        <li><a href="#">Admin</a><i class="fa fa-angle-right"></i></li>
                        <li class="current">Change News Cover Photo</li>
                    </ul>
                </div><!--//breadcrumbs-->
            </header>

            <!--Member Panel Start-->
            <div class="row">
                <?php
                if(isset($_SESSION) && !empty($_SESSION['message'])) {

                    $msg = Message::getMessage();

                    echo "
                        <p id='message' style='text-align: center; font-family: Pristina; color: red; font-size: 25px'>$msg</p>";

                }

                ?>
                <div class="col-md-12 ">
                    <form action="../../controller/coverPicEditAdmin.php" method="post" enctype="multipart/form-data">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 col-sm-offset-3 col-xs-offset-0">

                            <div class="form-group">

                                <img class="thumb" src="../../resources/news_photos/cover/<?php echo $oneNews->cover_pic?>" width="100%" alt="" />
                                <br><br><label>নিউজ কভার ফটোঃ (ছবি অবশ্যই ১৬ঃ৯ এস্পেক্ট রেশিও হতে হবে)</label>
                                <br>
                                <input type="file" class="form-control" name="cover_pic">
                                <input type="hidden" class="form-control" name="news_id" value="<?php echo $oneNews->id?>">
                            </div><!--//form-group-->
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Change Picture">
                            </div><!--//form-group-->
                        </div>

                    </form>
                </div>
            </div>
            <!--Member Panel End-->
            <hr>

        </div>
    </div><!--//content-->
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include ("../templateLayout/footer.php");?>


<?php include ("../templateLayout/script/templateScript.php");?>
<script src="//cdn.ckeditor.com/4.5.5/standard/ckeditor.js"></script>
<script> CKEDITOR.replace( 'text' );</script>
<script> CKEDITOR.replace( 'text2' );</script>
</body>
</html>

