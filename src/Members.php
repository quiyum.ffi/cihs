<?php
/**
 * Created by PhpStorm.
 * User: FFI
 * Date: 10/6/2017
 * Time: 7:34 PM
 */

namespace App;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class Members extends Database
{
    public $id;
    public $name;
    public $father_name;
    public $mother_name;
    public $spouse_name;
    public $date_of_birth;
    public $blood;
    public $nid_no;
    public $marital_status;
    public $permanent_address;
    public $present_address;
    public $contact;
    public $email;
    public $fb_id;
    public $occupation;
    public $organization;
    public $designation;
    public $admitted_class;
    public $left_class;
    public $passed_from_school;
    public $passing_year;
    public $batch_mate;
    public $batch_mate_contact;
    public $batch_mate_fb;
    public $status;
    public $reg_date;
    public $picture;
    public $last_id;
    public $reg_fee;
    public $gender;

    public function __construct(){
        parent::__construct();
    }

    public function prepareData($allPostData)
    {
        if (array_key_exists("member_id", $allPostData)) {
            $this->id = $allPostData['member_id'];
        }
        if (array_key_exists("member_name", $allPostData)) {
            $this->name = $allPostData['member_name'];
        }
        if (array_key_exists("father_name", $allPostData)) {
            $this->father_name = $allPostData['father_name'];
        }
        if (array_key_exists("mother_name", $allPostData)) {
            $this->mother_name = $allPostData['mother_name'];
        }
        if (array_key_exists("spouse_name", $allPostData)) {
            $this->spouse_name = $allPostData['spouse_name'];
        }
        if (array_key_exists("date_of_birth", $allPostData)) {
            $this->date_of_birth = $allPostData['date_of_birth'];
        }
        if (array_key_exists("blood_group", $allPostData)) {
            $this->blood = $allPostData['blood_group'];
        }
        if (array_key_exists("nid_no", $allPostData)) {
            $this->nid_no = $allPostData['nid_no'];
        }
        if (array_key_exists("marital", $allPostData)) {
            $this->marital_status = $allPostData['marital'];
        }
        if (array_key_exists("permanent_address", $allPostData)) {
            $this->permanent_address = $allPostData['permanent_address'];
        }
        if (array_key_exists("present_address", $allPostData)) {
            $this->present_address = $allPostData['present_address'];
        }
        if (array_key_exists("contact", $allPostData)) {
            $this->contact = $allPostData['contact'];
        }
        if (array_key_exists("email", $allPostData)) {
            $this->email = $allPostData['email'];
        }
        if (array_key_exists("fb_id", $allPostData)) {
            $this->fb_id = $allPostData['fb_id'];
        }

        if (array_key_exists("occupation", $allPostData)) {
            $this->occupation = $allPostData['occupation'];
        }
        if (array_key_exists("organization", $allPostData)) {
            $this->organization = $allPostData['organization'];
        }
        if (array_key_exists("designation", $allPostData)) {
            $this->designation = $allPostData['designation'];
        }
        if (array_key_exists("admitted_class", $allPostData)) {
            $this->admitted_class = $allPostData['admitted_class'];
        }
        if (array_key_exists("left_class", $allPostData)) {
            $this->left_class = $allPostData['left_class'];
        }
        if (array_key_exists("passed_from_school", $allPostData)) {
            $this->passed_from_school = $allPostData['passed_from_school'];
        }
        if (array_key_exists("passing_year", $allPostData)) {
            $this->passing_year = $allPostData['passing_year'];
        }
        if (array_key_exists("batch_mate", $allPostData)) {
            $this->batch_mate = $allPostData['batch_mate'];
        }
        if (array_key_exists("batch_mate_contact", $allPostData)) {
            $this->batch_mate_contact = $allPostData['batch_mate_contact'];
        }
        if (array_key_exists("batch_mate_fb", $allPostData)) {
            $this->batch_mate_fb = $allPostData['batch_mate_fb'];
        }
        if (array_key_exists("picture", $allPostData)) {
            $this->picture = $allPostData['picture'];
        }
        if (array_key_exists("reg_fee", $allPostData)) {
            $this->reg_fee = $allPostData['reg_fee'];
        }
        if (array_key_exists("gender", $allPostData)) {
            $this->gender = $allPostData['gender'];
        }


        return $this;

    }

    public function store(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $query= "INSERT INTO `members`(name,father_name,mother_name,spouse_name,birth_date,blood,nid_no,marital_status,permanent_adrs,present_adrs,contact,email,fb_id,occupation,organization,designation,admitted_class,left_class,passed_from_school,passing_year,batch_mate,batch_mate_contact,batch_mate_fb,picture,registration_date,reg_fee,gender) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->name);
        $STH->bindParam(2,$this->father_name);
        $STH->bindParam(3,$this->mother_name);
        $STH->bindParam(4,$this->spouse_name);
        $STH->bindParam(5,$this->date_of_birth);
        $STH->bindParam(6,$this->blood);
        $STH->bindParam(7,$this->nid_no);
        $STH->bindParam(8,$this->marital_status);
        $STH->bindParam(9,$this->permanent_address);
        $STH->bindParam(10,$this->present_address);
        $STH->bindParam(11,$this->contact);
        $STH->bindParam(12,$this->email);
        $STH->bindParam(13,$this->fb_id);
        $STH->bindParam(14,$this->occupation);
        $STH->bindParam(15,$this->organization);
        $STH->bindParam(16,$this->designation);
        $STH->bindParam(17,$this->admitted_class);
        $STH->bindParam(18,$this->left_class);
        $STH->bindParam(19,$this->passed_from_school);
        $STH->bindParam(20,$this->passing_year);
        $STH->bindParam(21,$this->batch_mate);
        $STH->bindParam(22,$this->batch_mate_contact);
        $STH->bindParam(23,$this->batch_mate_fb);
        $STH->bindParam(24,$this->picture);
        $STH->bindParam(25,$this->reg_date);
        $STH->bindParam(26,$this->reg_fee);
        $STH->bindParam(27,$this->gender);

        $STH->execute();
        $select_last_id="SELECT id FROM members ORDER BY id DESC limit 1";
        $STH = $this->DBH->query($select_last_id);

        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->execute();
        $row=$STH->fetch();
        $this->last_id= $row['id'];
        $_SESSION['last_id']=$this->last_id;
    }

    public function AdminStore(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $this->status=1;
        $query= "INSERT INTO `members`(name,father_name,mother_name,spouse_name,birth_date,blood,nid_no,marital_status,permanent_adrs,present_adrs,contact,email,fb_id,occupation,organization,designation,admitted_class,left_class,passed_from_school,passing_year,batch_mate,batch_mate_contact,batch_mate_fb,picture,status,registration_date,reg_fee,gender) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->name);
        $STH->bindParam(2,$this->father_name);
        $STH->bindParam(3,$this->mother_name);
        $STH->bindParam(4,$this->spouse_name);
        $STH->bindParam(5,$this->date_of_birth);
        $STH->bindParam(6,$this->blood);
        $STH->bindParam(7,$this->nid_no);
        $STH->bindParam(8,$this->marital_status);
        $STH->bindParam(9,$this->permanent_address);
        $STH->bindParam(10,$this->present_address);
        $STH->bindParam(11,$this->contact);
        $STH->bindParam(12,$this->email);
        $STH->bindParam(13,$this->fb_id);
        $STH->bindParam(14,$this->occupation);
        $STH->bindParam(15,$this->organization);
        $STH->bindParam(16,$this->designation);
        $STH->bindParam(17,$this->admitted_class);
        $STH->bindParam(18,$this->left_class);
        $STH->bindParam(19,$this->passed_from_school);
        $STH->bindParam(20,$this->passing_year);
        $STH->bindParam(21,$this->batch_mate);
        $STH->bindParam(22,$this->batch_mate_contact);
        $STH->bindParam(23,$this->batch_mate_fb);
        $STH->bindParam(24,$this->picture);
        $STH->bindParam(25,$this->status);
        $STH->bindParam(26,$this->reg_date);
        $STH->bindParam(27,$this->reg_fee);
        $STH->bindParam(28,$this->gender);

        $STH->execute();
    }
    public function view($id){
        $sql = "SELECT * FROM members WHERE id=".$id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function showMembers(){
        $sql = "Select * from members where status='1' ORDER BY passing_year ASC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showRequest(){
        $sql = "Select * from members where status='0' ORDER BY id DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showIgnoredRequest(){
        $sql = "Select * from members where status='2' ORDER BY id DESC";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function deleteMember(){
        $query="DELETE from members WHERE id=$this->id";
        $STH = $this->DBH->prepare($query);
        $STH->execute();
    }
    public function viewOne(){
        $sql = "Select * from members where id='$this->id'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function notification(){
        $sql = "SELECT count(status) as notification FROM `members` WHERE status=0";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function totalRegistared(){
        $sql = "SELECT COUNT(status) as numberRegistred FROM `members` WHERE status=1";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function approveMember(){
        $this->status=1;
        $query= "UPDATE members SET status=? WHERE id=$this->id";
        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1,$this->status);
        $STH->execute();
    }
    public function rejectMember(){
        $this->status=2;
        $query= "UPDATE members SET status=? WHERE id=$this->id";
        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1,$this->status);
        $STH->execute();
    }


}