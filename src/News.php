<?php
/**
 * Created by PhpStorm.
 * User: James Bond
 * Date: 7/6/2017
 * Time: 11:52 PM
 */

namespace App;
if(!isset($_SESSION) )  session_start();
use App\database\Database;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class News extends Database
{
    public $id;
    public $headline;
    public $news;
    public $admin_email;
    public $title_pic;
    public $cover_pic;
    public $date;
    public $newsId;
    public $status;

    public function __construct(){
        parent::__construct();
    }
    
    public function prepareData($allPostData)
    {
        if (array_key_exists("id", $allPostData)) {
            $this->id = $allPostData['id'];
        }
        if (array_key_exists("headline", $allPostData)) {
            $this->headline = $allPostData['headline'];
        }
        if (array_key_exists("details", $allPostData)) {
            $this->news = $allPostData['details'];
        }
        if (array_key_exists("admin_email", $allPostData)) {
            $this->admin_email = $allPostData['admin_email'];
        }
        if (array_key_exists("title_pic", $allPostData)) {
            $this->title_pic = $allPostData['title_pic'];
        }
        if (array_key_exists("cover_pic", $allPostData)) {
            $this->cover_pic = $allPostData['cover_pic'];
        }
        if (array_key_exists("news_id", $allPostData)) {
            $this->newsId = $allPostData['news_id'];
        }

    }

    public function store(){
        date_default_timezone_set('Asia/Dhaka');
        $date = date('Y-m-d H:i:s');
        $this->date=$date;
        $query= "INSERT INTO `news`(headline,news,admin_email,title_pic,cover_pic,date) VALUES (?,?,?,?,?,?)";

        $STH = $this->DBH->prepare($query);

        $STH->bindParam(1,$this->headline);
        $STH->bindParam(2,$this->news);
        $STH->bindParam(3,$this->admin_email);
        $STH->bindParam(4,$this->title_pic);
        $STH->bindParam(5,$this->cover_pic);
        $STH->bindParam(6,$this->date);

        $STH->execute();
    }
    
    
    public function updateTitleAdmin(){
        $query= "UPDATE news SET title_pic=? WHERE id=$this->newsId";
        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1,$this->title_pic);
        $STH->execute();
    }

    public function updateNews(){
        $arrData=array($this->headline,$this->news);
        $query= "UPDATE news SET headline=?,news=? WHERE id=$this->newsId";

        $STH = $this->DBH->prepare($query);

        $result=$STH->execute($arrData);
        if($result){
            Message::setMessage("Success! News Content has been update successfully!");
        }
        else{
            Message::setMessage("Failed! Information has not been updated!");
        }
        Utility::redirect('../views/moderator/news_details.php?news_id='.$this->newsId);
    }
    public function updateNewsAdmin(){
        $arrData=array($this->headline,$this->news);
        $query= "UPDATE news SET headline=?,news=? WHERE id=$this->newsId";

        $STH = $this->DBH->prepare($query);

        $result=$STH->execute($arrData);
        if($result){
            Message::setMessage("Success! News Content has been update successfully!");
        }
        else{
            Message::setMessage("Failed! Information has not been updated!");
        }
        Utility::redirect('../views/admin/news_details.php?news_id='.$this->newsId);
    }
    public function updateCoverAdmin(){
        $arrData=array($this->cover_pic);
        $query= "UPDATE news SET cover_pic=? WHERE id=$this->newsId";

        $STH = $this->DBH->prepare($query);

        $result=$STH->execute($arrData);
        if($result){
            Message::setMessage("Success! Cover photo has been update successfully!");
        }
        else{
            Message::setMessage("Failed! Information has not been updated!");
        }
        Utility::redirect('../views/admin/news_details.php?news_id='.$this->newsId);
    }



    public function showTopNews(){
        $sql = "Select news.id,news.headline,news.news,news.title_pic,news.cover_pic,news.date,admin.name as admin_name from news,admin WHERE news.admin_email=admin.email AND news.status='1' ORDER BY id DESC limit 0,1";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showallNews(){
        $sql = "Select news.id,news.headline,news.news,news.title_pic,news.cover_pic,news.date,admin.name as admin_name from news,admin WHERE news.admin_email=admin.email AND news.status='1' ORDER BY id DESC limit 1,6";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showOtherNews(){
        $sql = "Select news.id,news.headline,news.news,news.title_pic,news.cover_pic,news.date,admin.name as admin_name from news,admin WHERE news.admin_email=admin.email AND news.status='1' ORDER BY id DESC limit 7,12";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showallNewsSidebar(){
        $sql = "Select * FROM news WHERE NOT news.id='$this->newsId' AND news.status='1' ORDER BY id DESC limit 0,10";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showallNewsSidebarHome(){
        $sql = "Select * from news WHERE news.status='1' ORDER BY id DESC limit 0,15";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function showdeletedNews(){
        $sql = "Select news.id,news.headline,news.news,news.title_pic,news.cover_pic,news.date,admin.name as admin_name from news,admin WHERE news.admin_email=admin.email AND news.status='0' ORDER BY id DESC limit 0,20";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }


    public function showSingleData(){
        $sql = "Select news.id,news.headline,news.news,news.title_pic,news.cover_pic,news.date,admin.name as admin_name,news.status  from news,admin WHERE news.admin_email=admin.email AND news.id=$this->newsId";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function delete(){
        $this->status='0';
        $query= "UPDATE news SET status =? WHERE id=$this->newsId";
        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1,$this->status);
        $STH->execute();
    }
    public function restore(){
        $this->status='1';
        $query= "UPDATE news SET status =? WHERE id=$this->newsId";
        $STH = $this->DBH->prepare($query);
        $STH->bindParam(1,$this->status);
        $STH->execute();
    }

    public function shownewsActive(){
        $sql = "Select news.id,news.headline,news.news,news.title_pic,news.cover_pic,news.date,admin_info.name as mod_email from news,admin_info WHERE news.mod_email=admin_info.email ORDER BY id DESC limit 0,3";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function shownewsHome(){
        $sql = "Select news.id,news.headline,news.news,news.title_pic,news.cover_pic,news.date,admin_info.name as mod_email from news,admin_info WHERE news.mod_email=admin_info.email ORDER BY id DESC limit 3,3";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }




}