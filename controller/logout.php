<?php
if(!isset($_SESSION) )session_start();
include_once('../vendor/autoload.php');
use App\Admin;
use App\Message\Message;
use App\Utility\Utility;

$auth= new Admin();
$status= $auth->log_out();

session_destroy();
session_start();

Message::setMessage("Success! You Logout successfully!");
Utility::redirect('../login.php');