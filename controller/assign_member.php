<?php
require_once("../vendor/autoload.php");
use App\Message\Message;
use App\Utility\Utility;
use App\Members;
$object=new Members();
$_POST['date_of_birth']=$_POST['year']."-".$_POST['month']."-".$_POST['date'];
$_POST['permanent_address']="Vill: ".$_POST['vill'].", Post Office: ".$_POST['post_office'].", Police Station: ".$_POST['police_station'].", District: ".$_POST['district'];
$_POST['batch_mate']=implode(",",$_POST['batch_mate']);
$_POST['batch_mate_contact']=implode(",",$_POST['batch_mate_contact']);
$_POST['batch_mate_fb']=implode(",",$_POST['batch_mate_fb']);
if($_POST['passing_year']>=2002 && $_POST['passing_year']<=2010){
    $amount=1000;
}
else if($_POST['passing_year']>=2011 && $_POST['passing_year']<=2017){
    $amount=500;
}
else if($_POST['passing_year']>=9999){
    $amount=200;
}
$_POST['reg_fee']=$amount;
$object->prepareData($_POST);
$object->AdminStore();
Message::setMessage("Registration has been completed!");
Utility::redirect('../admin/assign_member.php');