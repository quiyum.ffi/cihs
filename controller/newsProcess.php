<?php
require_once '../vendor/autoload.php';
use App\News;
use App\Utility\Utility;
use App\Message\Message;
$object=new News();
$object->prepareData($_POST);
$object->store();
Message::setMessage("News has been added successfully!");
return Utility::redirect('../admin/news.php');
