<?php
require_once("../vendor/autoload.php");
use App\Members;
use App\Message\Message;
use App\Utility\Utility;
$object= new Members();
$object->prepareData($_GET);
$object->deleteMember();
Message::setMessage("Member request has been deleted permanently!");
Utility::redirect('../admin/ignored_requests.php');
?>