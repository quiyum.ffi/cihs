<?php
if(!isset($_SESSION) )session_start();
include_once('../vendor/autoload.php');
use App\Admin;
use App\Utility\Utility;
use App\Message\Message;
$auth= new Admin();

$admin= $auth->prepareData($_POST)->admin();

if($admin){
    $_SESSION['role_status']=0;
    $_SESSION['email']=$_POST['email'];
    Message::setMessage("Success! You have log in Successfully!");
    Utility::redirect('../admin/admin_profile.php');
}
else{
    Message::setMessage("Email and password doesn't match!");
    Utility::redirect('../login.php');

}


