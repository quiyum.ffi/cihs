<?php
$base_url=base_url;
$memberRequest=$base_url.'views/admin/member_requests.php';
$assignMember=$base_url.'views/admin/assignMember.php';
$authMember=$base_url.'views/admin/adviser&executive.php';
$profile=$base_url.'views/admin/adminProfile.php';
$authen=$base_url.'views/admin/authenticateMember.php';
$reset=$base_url.'views/admin/resetPassword.php';
$pic=$base_url.'views/admin/changeProfilePic.php';
$delete=$base_url.'views/admin/deletedMember.php';
$ignore=$base_url.'views/admin/ignoredRequest.php';
$deleteProfile=$base_url.'views/admin/deletedProfile.php';
$mod=$base_url.'views/admin/addModerator.php';


$url ='http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
if($url==$memberRequest){
    $memberClass="active";
}
elseif($url==$assignMember){
    $assignClass="active";
}
elseif($url==$authMember){
    $authClass="active";
}
elseif($url==$authen){
    $authClass="active";
}
elseif($url==$profile){
    $profileClass="active";
}
elseif($url==$reset){
    $profileClass="active";
}
elseif($url==$pic){
    $profileClass="active";
}
elseif($url==$delete){
    $memberClass="active";
}
elseif($url==$deleteProfile){
    $memberClass="active";
}
elseif($url==$ignore){
    $memberClass="active";
}
elseif($url==$mod){
    $modClass="active";
}



?>
<header class="header">
    <div class="top-bar">
        <div class="container">
            <ul class="social-icons col-md-6 col-sm-6 col-xs-12 hidden-xs">
                <li><a href="#" ><i class="fa fa-twitter"></i></a></li>
                <li><a href="https://www.facebook.com/tarunno.arl.pg/" target="_blank" ><i class="fa fa-facebook"></i></a></li>
                <li><a href="#" ><i class="fa fa-youtube"></i></a></li>
                <li><a href="#" ><i class="fa fa-google-plus"></i></a></li>
                <li class="row-end"><a href="#" ><i class="fa fa-rss"></i></a></li>
            </ul><!--//social-icons-->
            <form class="pull-right search-form" role="search">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search the site...">
                </div>
                <button type="submit" class="btn btn-theme">Go</button>
            </form>
        </div>
    </div><!--//to-bar-->
    <div class="header-main container">
        <h1 class="logo col-md-4 col-sm-4">
            <a href="<?php echo base_url;?>views/index.php"><img id="logo" src="<?php echo base_url;?>resources/assets/images/logo.png" alt="Logo"></a>
        </h1><!--//logo-->
        <div class="info col-md-8 col-sm-8">
            <ul class="menu-top navbar-right hidden-xs">
                <li class="divider"><a href="<?php echo base_url;?>views/index.php">Home</a></li>
                <li><a href="<?php echo base_url;?>views/contact.php">Contact</a></li>
            </ul><!--//menu-top-->


        </div><!--//info-->
    </div><!--//header-main-->
</header><!--//header-->

<!-- ******NAV****** -->
<nav class="main-nav" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button><!--//nav-toggle-->
        </div><!--//navbar-header-->

        <div class="navbar-collapse collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="nav-item <?php echo $profileClass;?>"><a href="<?php echo base_url;?>views/moderator/moderatorProfile.php">Profile</a></li>
                <li class="nav-item dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">associate <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url;?>views/moderator/adviser&executive.php"">Adviser & Executive</a></li>
                        <li><a href="<?php echo base_url;?>views/moderator/authMembers.php"">Members</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">News And Events<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url;?>views/moderator/news.php"> News</a></li>
                        <li><a href="<?php echo base_url;?>views/moderator/events.php"> Events</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown <?php echo $pollClass;?>"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Poll <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url;?>views/moderator/poll.php"">New Poll</a></li>
                        <li><a href="<?php echo base_url;?>views/moderator/closedPoll.php"">Closed Poll</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Update Status <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url;?>views/moderator/addNews.php">Add News</a></li>
                        <li><a href="<?php echo base_url;?>views/moderator/events.php">Add Events</a></li>
                    </ul>
                </li>
                <li class="nav-item"><a href="<?php echo base_url; ?>controller/authentication/logout.php"">Log Out</a></li>
            </ul><!--//nav-->
        </div><!--//navabr-collapse-->
    </div><!--//container-->
</nav><!--//main-nav-->
