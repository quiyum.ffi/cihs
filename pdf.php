<?php
require_once __DIR__ . '../vendor/autoload.php';
session_start();
require_once("vendor/autoload.php");
include("templateLayout/templateInformation.php");
use App\Members;
$object=new Members();
$oneData=$object->view($_SESSION['last_id']);
if($oneData->passing_year>=2002 && $oneData->passing_year<=2010){
    $passing_year=$oneData->passing_year;
    $amount=1000;
}
else if($oneData->passing_year>=2011 && $oneData->passing_year<=2017){
    $amount=500;
    $passing_year=$oneData->passing_year;
}
else if($oneData->passing_year>=0000){
    $amount=200;
    $passing_year="Regular";
}
$date=$oneData->registration_date;
$new=date('Y-m-d H:i:s',strtotime('+72 hour +0 minutes',strtotime($date)));
$newdate=date('d/m/Y h:i:s a', strtotime($new));
$date = date("d", strtotime("$oneData->birth_date"));
$month = date("m", strtotime("$oneData->birth_date"));
$year = date("Y", strtotime("$oneData->birth_date"));
if($month=='01'){
    $monthD='January';
}
if($month=='02'){
    $monthD='February';
}
else if($month=='03'){
    $monthD='March';
}
else if($month=='04'){
    $monthD='April';
}
else if($month=='05'){
    $monthD='May';
}
else if($month=='06'){
    $monthD='June';
}
else if($month=='07'){
    $monthD='July';
}
else if($month=='08'){
    $monthD='August';
}
else if($month=='09'){
    $monthD='September';
}
else if($month=='10'){
    $monthD='October';
}
else if($month=='11'){
    $monthD='November';
}
else if($month=='12'){
    $monthD='December';
}
if($date==01)
{$sts="st";}
elseif($date==31)
{$sts="st";}
elseif($date==2)
{$sts="nd";}
elseif($date==3)
{$sts="rd";}
else
{$sts="th";}
if($oneData->status==1){
    $regStatus="Registration has been completed";
}
elseif($oneData->status==1){
    $regStatus="Registration has been completed";
}
elseif($oneData->status==2){
    $regStatus="Registration request has been rejected!";
}
elseif($oneData->status==0){
    $regStatus="Your registration is on processing!";
}
$html=<<<EOD
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title>$title;</title>
     <?php include("templateLayout/css/meta.php");?>
    <link href="resources/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
   
</head>

<body style="style="font-family: Agency FB; font-size: 10pt;">
<div class="wrapper">
    <div class="content container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2">
                    <img src="resources/banner.jpg" class="img-responsive img-rounded">
                    <h4 style="text-align:center">Reunion-2018</h3>
                    <h5 class="text-center">(Date: 06<sup>th</sup> January, 2018)</h4>
                </div>
            </div>
            <div style="width: 100%;overflow: hidden">
                <div style="width: 100%;overflow: hidden">
                    <div style="width: 30%;float: left">
                        <img src="resources/members_photo/$oneData->picture" class="img-responsive img-rounded">
                    </div>
                 
                </div>

                <div style="width: 100%;margin-top:30px;overflow: hidden">
                     <table border="2px black solid" style="width: 100%;">
                      <tr>
                            <td style="width:40%">Reference No</td>
                            <td style="text-align: right">$oneData->id</td>
                        </tr>
                        <tr>
                            <td style="width:40%">Name</td>
                            <td style="text-align: right">$oneData->name</td>
                        </tr>
                         
                   
                        <tr>
                            <td>Passing Year</td>
                            <td style="text-align: right">$passing_year</td>
                        </tr>
                        <tr>
                            <td>Payable Amount</td>
                            <td style="text-align: right">$amount</td>
                        </tr>
                        <tr>
                            <td style="width:40%">Mother's Name</td>
                            <td style="text-align: right">$oneData->mother_name</td>
                        </tr>
                        <tr>
                            <td>Father's Name</td>
                            <td style="text-align: right">$oneData->father_name</td>
                        </tr>
                        <tr>
                            <td>Date of Birth</td>
                            <td style="text-align: right">$date<sup>$sts</sup> $monthD $year</td>
                        </tr>
                        <tr>
                            <td>Contact</td>
                            <td style="text-align: right">$oneData->contact</td>
                        </tr>
                        
                        <tr>
                            <td>Email</td>
                            <td style="text-align: right">$oneData->email</td>
                        </tr>
                         <tr>
                            <td>Blood Group</td>
                            <td style="text-align: right">$oneData->blood</td>
                        </tr>
                        <tr>
                            <td>Permanent Address</td>
                            <td style="text-align: right">$oneData->permanent_adrs</td>
                        </tr>
                         <tr>
                            <td>Present Address</td>
                            <td style="text-align: right">$oneData->present_adrs</td>
                        </tr>
                      
                     
                        <tr>
                            <td>Marital Status</td>
                            <td style="text-align: right">$oneData->marital_status</td>
                        </tr>
                        <tr>
                            <td>Facebook ID</td>
                            <td style="text-align: right">$oneData->fb_id</td>
                        </tr>
                        <tr>
                            <td>Admitted Class</td>
                            <td style="text-align: right">$oneData->admitted_class</td>
                        </tr>
                        <tr>
                            <td>Left Class</td>
                            <td style="text-align: right">$oneData->left_class</td>
                        </tr>
                        <tr>
                            <td>Registration Date</td>
                            <td style="text-align: right">$oneData->registration_date</td>
                        </tr>
                    </table>
                </div>
                <div style="margin-top:15px">
                <h4 style="text-align:center;color:red">$regStatus</h3>
                 <h5 style="text-align:center;> Please contact with us for any information</h5>
                 <p style="font-size: 13px;text-align:center;>Contact No:+88-01817-748970, +88-01841-095018, +88-01826-132308</p>
                </div>
                <div>
                <hr>
               <h5 style="text-align:center;">Technical Support: Future Features of IT. 156 CDA Avenue, East Nasirabad, Chittagong. website: www.ffibd.com</h5>
                </div>
            </div>
        </div>
    </div><!--//content-->
</div><!--//wrapper-->
</body>
</html>

EOD;


$mpdf = new \Mpdf\Mpdf();
$mpdf->SetDisplayMode('fullwidth');


// Write some HTML code:

$mpdf->WriteHTML($html);


// Output a PDF file directly to the browser
$mpdf->Output("$oneData->name.pdf",'D');


