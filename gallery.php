<?php
require_once "vendor/autoload.php";
include("templateLayout/templateInformation.php");

?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("templateLayout/css/meta.php");?>
    <?php include("templateLayout/css/templateCss.php");?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
    <link rel="stylesheet" href="resources/assets/css/thumbnail-gallery.css">

</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include("templateLayout/headerAndNavigation.php");?>
    <!-- ******CONTENT****** -->
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left">Gallery</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">You are here:</li>
                        <li><a href="index.php">Home</a><i class="fa fa-angle-right"></i></li>
                        <li class="current">Gallery</li>
                    </ul>
                </div><!--//breadcrumbs-->
            </header>
            <div class="page-content">
                <div class="row page-row">
                    <div class="container gallery-container" style="margin-top: -65px">

                        <h1>Photo Gallery</h1>



                        <div class="tz-gallery">

                            <div class="row">

                                <div class="col-xs-6 col-sm-4 col-md-4">
                                    <div class="thumbnail">
                                        <a class="lightbox" href="resources/gallery_image/park.jpg">
                                            <img src="resources/gallery_image/park.jpg" alt="Park">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-4">
                                    <div class="thumbnail">
                                        <a class="lightbox" href="resources/gallery_image/park.jpg">
                                            <img src="resources/gallery_image/park.jpg" alt="Park">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-4">
                                    <div class="thumbnail">
                                        <a class="lightbox" href="resources/gallery_image/park.jpg">
                                            <img src="resources/gallery_image/park.jpg" alt="Park">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-4">
                                    <div class="thumbnail">
                                        <a class="lightbox" href="resources/gallery_image/park.jpg">
                                            <img src="resources/gallery_image/park.jpg" alt="Park">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-4">
                                    <div class="thumbnail">
                                        <a class="lightbox" href="resources/gallery_image/park.jpg">
                                            <img src="resources/gallery_image/park.jpg" alt="Park">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-4">
                                    <div class="thumbnail">
                                        <a class="lightbox" href="resources/gallery_image/park.jpg">
                                            <img src="resources/gallery_image/park.jpg" alt="Park">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-4">
                                    <div class="thumbnail">
                                        <a class="lightbox" href="resources/gallery_image/park.jpg">
                                            <img src="resources/gallery_image/park.jpg" alt="Park">
                                        </a>
                                    </div>
                                </div><div class="col-xs-6 col-sm-4 col-md-4">
                                    <div class="thumbnail">
                                        <a class="lightbox" href="resources/gallery_image/park.jpg">
                                            <img src="resources/gallery_image/park.jpg" alt="Park">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-4">
                                    <div class="thumbnail">
                                        <a class="lightbox" href="resources/gallery_image/park.jpg">
                                            <img src="resources/gallery_image/park.jpg" alt="Park">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-4">
                                    <div class="thumbnail">
                                        <a class="lightbox" href="resources/gallery_image/park.jpg">
                                            <img src="resources/gallery_image/park.jpg" alt="Park">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-4">
                                    <div class="thumbnail">
                                        <a class="lightbox" href="resources/gallery_image/park.jpg">
                                            <img src="resources/gallery_image/park.jpg" alt="Park">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-4">
                                    <div class="thumbnail">
                                        <a class="lightbox" href="resources/gallery_image/park.jpg">
                                            <img src="resources/gallery_image/park.jpg" alt="Park">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-4">
                                    <div class="thumbnail">
                                        <a class="lightbox" href="resources/gallery_image/park.jpg">
                                            <img src="resources/gallery_image/park.jpg" alt="Park">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-4">
                                    <div class="thumbnail">
                                        <a class="lightbox" href="resources/gallery_image/park.jpg">
                                            <img src="resources/gallery_image/park.jpg" alt="Park">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-4">
                                    <div class="thumbnail">
                                        <a class="lightbox" href="resources/gallery_image/park.jpg">
                                            <img src="resources/gallery_image/park.jpg" alt="Park">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-4">
                                    <div class="thumbnail">
                                        <a class="lightbox" href="resources/gallery_image/park.jpg">
                                            <img src="resources/gallery_image/park.jpg" alt="Park">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-4">
                                    <div class="thumbnail">
                                        <a class="lightbox" href="resources/gallery_image/park.jpg">
                                            <img src="resources/gallery_image/park.jpg" alt="Park">
                                        </a>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-4">
                                    <div class="thumbnail">
                                        <a class="lightbox" href="resources/gallery_image/park.jpg">
                                            <img src="resources/gallery_image/park.jpg" alt="Park">
                                        </a>
                                    </div>
                                </div>


                            </div>

                        </div>

                    </div>
                </div><!--//page-row-->
            </div><!--//page-content-->
        </div><!--//page-->
    </div><!--//content-->
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include("templateLayout/footer.php");?>


<?php include("templateLayout/script/templateScript.php");?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script>
    baguetteBox.run('.tz-gallery');
</script>

</body>
</html>

