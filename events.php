<?php
require_once "vendor/autoload.php";
include("templateLayout/templateInformation.php");
use App\Members;
$object=new Members();
$registered=$object->totalRegistared();
function bfn($str) {
    $search=array("0","1","2","3","4","5",'6',"7","8","9");
    $replace=array("০","১","২","৩","৪","৫",'৬',"৭","৮","৯");
    return str_replace($search,$replace,$str);
}
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title><?php echo $title;?></title>
    <?php include("templateLayout/css/meta.php");?>
    <?php include("templateLayout/css/templateCss.php");?>

</head>

<body class="home-page">
<div class="wrapper">
    <!-- ******HEADER****** -->
    <?php include("templateLayout/headerAndNavigation.php");?>
    <!-- ******CONTENT****** -->
    <div class="content container">
        <div class="page-wrapper">
            <header class="page-heading clearfix">
                <h1 class="heading-title pull-left">Events</h1>
                <div class="breadcrumbs pull-right">
                    <ul class="breadcrumbs-list">
                        <li class="breadcrumbs-label">You are here:</li>
                        <li><a href="index.php">Home</a><i class="fa fa-angle-right"></i></li>
                        <li class="current">Events</li>
                    </ul>
                </div><!--//breadcrumbs-->
            </header>
            <div class="page-content">
                <div class="row page-row">
                    <div class="news-wrapper col-md-8 col-sm-7">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <img src="resources/img/reunion.jpg" class="img-responsive">
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <article class="news-item page-row has-divider clearfix row">
                                            <h3 class="title"><a href="news_details.php?news_id=<?php echo $oneNews->id?>">রি-ইউনিয়ন ২০১৮</a></h3>
                                            <p>আগামী ৬ই জানুয়ারী ২০১৮ তে অনুষ্ঠিত হতে যাচ্ছে রিইউনিয়ন ২০১৮। প্রথমবারের মত চিটাগাং আইডিয়্যাল হাই স্কুল প্রাক্তন ছাত্র-ছাত্রী পরিষদের মাধ্যমে আয়োজিত এ অনুষ্ঠানে উপস্থিত থাকবেন ২০০২ সাল থেকে ২০১৭ সাল পর্যন্ত সকল ছাত্র ছাত্রী, শিক্ষক-শিক্ষিকা ও অতিথিবৃন্দ। অনুষ্ঠানে সাংস্কৃতিক অনুষ্ঠানের প্রধান আকর্ষন থাকছেন মিরাক্কেল আক্কেল চ্যালেঞ্জার ৯ এর রানার আপ আরমান এবং আরেক মিরাক্কেলিয়ান ইয়াকুব রাসেল। এছাড়াও থাকছে স্কুলের ছাত্র-ছাত্রীদের অংশগ্রহনে মনোজ্ঞ সাংস্কৃতিক অনুষ্ঠান।</p>
                                            <a class="btn btn-theme read-more" href="registration.php">রি-ইউনিয়নের জন্য রেজিস্ত্রেশন করুন<i class="fa fa-chevron-right"></i></a>

                                </article><!--//news-item-->
                                <h5 class="text-center">রি-ইউনিয়ন ২০১৮ এর জন্য  ইতিমধ্যেই রেজিষ্ট্রেশন করেছেনঃ <strong><?php echo bfn($registered->numberRegistred);?> </strong>জন</h5>
                            </div>
                        </div>
                    </div><!--//news-wrapper-->
                    <aside class="page-sidebar  col-md-3 col-md-offset-1 col-sm-4 col-sm-offset-1">
                        <section class="widget has-divider">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <br>
                                <h3 class="title">Like Our Facebook Page</h3>
                                <iframe src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FChittagong-Ideal-High-School%2F393256520740208&width=260&layout=standard&action=like&size=small&show_faces=true&share=true&height=80&appId" width="100%" height="80" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                            </div>
                        </section><!--//widget-->
                        <section class="widget has-divider">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <br>
                                <h3 class="title">Upcoming Events</h3>
                                <article class="events-item page-row row">
                                    <div class="date-label-wrapper col-md-3 col-sm-4 col-xs-4">
                                        <p class="date-label">
                                            <span class="month">06</span>
                                            <span class="date-number" style="font-size: 10px">January</span>
                                            <span class="date-number" style="font-size: 10px">2018</span>
                                        </p>
                                    </div><!--//date-label-wrapper-->
                                    <div class="details col-md-9 col-sm-8 col-xs-8">
                                        <h3 class="title">Reunion 2018</h3>
                                        <p class="time"><i class="fa fa-clock-o"></i>09:00am to 05:00pm</p>
                                        <p class="location"><i class="fa fa-map-marker"></i>Chittagong Ideal High School</p>
                                    </div><!--//details-->
                                </article>
                            </div>


                        </section><!--//widget-->

                    </aside>
                </div><!--//page-row-->
            </div><!--//page-content-->
        </div><!--//page-->
    </div><!--//content-->
</div><!--//wrapper-->

<!-- ******FOOTER****** -->
<?php include("templateLayout/footer.php");?>


<?php include("templateLayout/script/templateScript.php");?>

</body>
</html>

